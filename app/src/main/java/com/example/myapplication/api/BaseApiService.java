package com.example.myapplication.api;


import com.example.myapplication.model.ResponseLogin;
import com.example.myapplication.model.ResponseRegister;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
public interface BaseApiService {

    @FormUrlEncoded
    @POST("login")
    Call<ResponseLogin> loginRequest(@Field("email") String email,
                                     @Field("password") String password);

    @FormUrlEncoded
    @POST("register")
    Call<ResponseRegister> registerRequest(@Field("username") String username,
                                           @Field("email") String email,
                                           @Field("password") String password);
}
