package com.example.myapplication.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.SharedPrefManager;

public class MainActivity extends AppCompatActivity {
    private static int SPLASH_SCREEN = 3000;
    SharedPrefManager sharedPrefManager;
    Context mContext;
    Animation imgAnim, txtAnim;
    ImageView image;
    TextView tvJudul;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        mContext = this;

        imgAnim = AnimationUtils.loadAnimation(this,R.anim.image_animation);
        txtAnim = AnimationUtils.loadAnimation(this,R.anim.text_animation);

        image = findViewById(R.id.imageView);
        tvJudul = findViewById(R.id.tvJudul);

        image.setAnimation(imgAnim);
        tvJudul.setAnimation(txtAnim);

        sharedPrefManager = new SharedPrefManager(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (sharedPrefManager.getSPSudahLogin()) {
                    startActivity(new Intent(MainActivity.this, Home.class));
                    finish();
                } else {
                    startActivity(new Intent(mContext, LoginActivity.class));
                    finish();
                }
            }
        },SPLASH_SCREEN);
    }
}
