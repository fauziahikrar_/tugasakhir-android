package com.example.myapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.SharedPrefManager;
import com.example.myapplication.api.BaseApiService;
import com.example.myapplication.api.UtilsApi;
import com.example.myapplication.model.ResponseLogin;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText InputEmail, InputPassword;
    private Button ButtonLogin;
    private TextView txtRegister;

    Context mContext;
    BaseApiService mApiService;
    SharedPrefManager sharedPrefManager;
    ProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtRegister = (TextView) findViewById(R.id.txtRegister);
        txtRegister.setOnClickListener(this);

        mContext = this;
        mApiService = UtilsApi.getAPIService();
        sharedPrefManager = new SharedPrefManager(this);

        initComponents();
    }

    private void initComponents() {
        InputEmail = (EditText) findViewById(R.id.input_email);
        InputPassword = (EditText) findViewById(R.id.input_password);
        ButtonLogin = (Button) findViewById(R.id.btn_login);

        ButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading = ProgressDialog.show(mContext, null, "Harap Tunggu...", true, false);
                requestLogin();
            }

            private void requestLogin() {
                mApiService.loginRequest(InputEmail.getText().toString(), InputPassword.getText().toString()).enqueue(new Callback<ResponseLogin>() {
                    @Override
                    public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                        Log.e("lamongan","token : " + response.body().getToken());

                        sharedPrefManager.saveSPString(SharedPrefManager.SP_TOKEN, "Bearer " + response.body().getToken());
                        sharedPrefManager.saveSPBoolean(SharedPrefManager.SP_SUDAH_LOGIN, true);
                        startActivity(new Intent(mContext, Home.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();

                        Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<ResponseLogin> call, Throwable t) {
                        Log.e("lamongan", t.getMessage());
                    }
                });
            }
        });
    }


    @Override
    public void onClick(View v) {
        Intent Register=new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(Register);
        finish();

    }
}
