package com.example.myapplication.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.example.myapplication.R;

public class Home extends AppCompatActivity implements View.OnClickListener {

    private CardView rute,posko,laporbanjir,laporbantuan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        rute = (CardView) findViewById(R.id.rute);
        posko = (CardView) findViewById(R.id.posko);
        laporbanjir = (CardView) findViewById(R.id.laporbanjir);
        laporbantuan = (CardView) findViewById(R.id.laporbantuan);

        rute.setOnClickListener(this);
        posko.setOnClickListener(this);
        laporbanjir.setOnClickListener(this);
        laporbantuan.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        Intent i;

        switch (v.getId()){
            case R.id.rute : i = new Intent(this, com.example.myapplication.activity.rute.class); startActivity(i); break;
            case R.id.posko : i = new Intent(this, com.example.myapplication.activity.posko.class); startActivity(i); break;
            case R.id.laporbanjir : i = new Intent(this, com.example.myapplication.activity.laporbanjir.class); startActivity(i); break;
            case R.id.laporbantuan : i = new Intent(this, com.example.myapplication.activity.laporbantuan.class); startActivity(i); break;
            default:break;
        }

    }
}
