package com.example.myapplication.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.SharedPrefManager;
import com.example.myapplication.api.BaseApiService;
import com.example.myapplication.api.UtilsApi;
import com.example.myapplication.model.ResponseRegister;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {
    private EditText  InputUsername, InputEmail, InputPassword;
    private Button ButtonRegister;

    Context mContext;
    BaseApiService mApiService;
    SharedPrefManager sharedPrefManager;
    ProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mContext = this;
        mApiService = UtilsApi.getAPIService();
        sharedPrefManager = new SharedPrefManager(this);

        initComponents();
    }

    private void initComponents() {
        InputUsername = (EditText) findViewById(R.id.et_uname);
        InputEmail = (EditText) findViewById(R.id.et_email);
        InputPassword = (EditText) findViewById(R.id.et_password);
        ButtonRegister = (Button) findViewById(R.id.btn_register);

        ButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading = ProgressDialog.show(mContext, null, "Harap Tunggu...", true, false);
                requestRegister();
            }

            private void requestRegister() {
                mApiService.registerRequest(InputUsername.getText().toString(), InputEmail.getText().toString(), InputPassword.getText().toString()).enqueue(new Callback<ResponseRegister>() {
                    @Override
                    public void onResponse(Call<ResponseRegister> call, Response<ResponseRegister> response) {
                        Log.e("lamongan","msg : " + response.body().getMessage());
                        Toast.makeText(RegisterActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(mContext, LoginActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                        finish();

                    }

                    @Override
                    public void onFailure(Call<ResponseRegister> call, Throwable t) {
                        Log.e("lamongan", t.getMessage());
                    }
                });
            }

        });
    }
}
