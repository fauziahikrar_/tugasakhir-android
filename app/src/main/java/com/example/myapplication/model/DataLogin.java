package com.example.myapplication.model;

import com.google.gson.annotations.SerializedName;

public class DataLogin {
	@SerializedName("id")
	private int id;
	@SerializedName("email")
	private String email;
	@SerializedName("username")
	private String username;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}
