package com.example.myapplication.model;

import com.google.gson.annotations.SerializedName;

public class ResponseLogin {
	@SerializedName("dataLogin")
	private DataLogin dataLogin;
	@SerializedName("message")
	private String message;
	@SerializedName("token")
	private String token;

	public void setDataLogin(DataLogin dataLogin){
		this.dataLogin = dataLogin;
	}

	public DataLogin getDataLogin(){
		return dataLogin;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"data = '" + dataLogin + '\'' +
			",message = '" + message + '\'' + 
			",token = '" + token + '\'' + 
			"}";
		}
}
