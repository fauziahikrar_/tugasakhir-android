package com.example.myapplication.model;

import com.google.gson.annotations.SerializedName;

public class ResponseRegister {
	@SerializedName("dataRegister")
	private DataRegister dataRegister;
	@SerializedName("message")
	private String message;

	public void setDataRegister(DataRegister dataRegister){
		this.dataRegister = dataRegister;
	}

	public DataRegister getDataRegister(){
		return dataRegister;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"data = '" + dataRegister + '\'' +
			",message = '" + message + '\'' +
			"}";
		}
}
